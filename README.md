# 1420MHz (hydrogen-line)


## How to run:
Just cd to project `server` or `client` and hit cargo run.
If you want to change IP you have to change const `IP` in `client/networking.rs` file

## Screenshots:
![asteroids](docs/assets/screenshot-1712400431617.png)
![other player](docs/assets/screenshot-1712400448909.png)
![player radar](docs/assets/screenshot-1712409250992.png)
![player firing](docs/assets/screenshot-1712497254337.png)
![player explosion](docs/assets/screenshot-1712497264057.png)
![turrets and chat](docs/assets/screenshot-1730932730441.png)
![many players and position text](docs/assets/screenshot-1731074074343.png)

## How to cross-build to Windows:
<https://bevy-cheatbook.github.io/setup/cross/linux-windows.html>
```sh
# To setup build:
rustup target add x86_64-pc-windows-gnu
# also install required `mingw-w64` packages on your distro, it will throw error on building app otherwise

# To build:
cargo build --target=x86_64-pc-windows-gnu --release
```

## Releases folder
Will not contain actual recent releases, but will always be guaranteed to work.
Use when you want to try the game, but it's best to simply run the build.

## Credits:
- "Space Kit" https://www.kenney.nl/assets/space-kit by Kenney is licensed under CC0
- "dingus the cat" (https://skfb.ly/oAtMJ) by bean(alwayshasbean) is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).



## State machine:

+ AssetLoading
  - Loading
  - Loaded
    + GameMode
      - StartMenu
        - Hangar
        - MultiplayerBattleLoading
          - MultiplayerBattle
            - BattleCameraState::Present