use bevy::{core_pipeline::bloom::BloomSettings, input::mouse::MouseMotion, prelude::*};

use bevy::window::{CursorGrabMode, PrimaryWindow};
use hydrogen_line_shared::structs::value_in_range::ValueInRange;
use hydrogen_line_shared::FacingRotation;

use crate::replication::inits::init_players;
use crate::{GameMode, MyShip, UiCamera};

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<CameraZoom>()
            .add_systems(
                PreUpdate,
                (setup_camera)
                    .after(init_players)
                    .run_if(in_state(GameMode::MultiplayerBattleLoading)),
            )
            .add_systems(PostStartup, turn_off_mouse_visibility)
            .add_systems(Update, (toggle_mouse_visibility, camera_zoom))
            .add_systems(
                Update,
                (camera_movement, update_facing_rotation)
                    .chain()
                    .run_if(in_state(GameMode::MultiplayerBattle)),
            )
            .add_systems(
                PostUpdate,
                (camera_follow_player).run_if(in_state(GameMode::MultiplayerBattle)),
            );
    }
}

/// Singleton
#[derive(Component, Default, Debug, Clone, Copy)]
pub struct CameraCradle;

pub fn setup_camera(
    mut commands: Commands,
    // HACK: This query is required soo this setup runs only once after player's ship spawns
    //   Probably change this to something using Events, cause now this system spins in place
    player: Query<(), Added<MyShip>>,
) {
    if player.get_single().is_ok() {
        let camera = commands
            .spawn((
                Camera3dBundle {
                    camera: Camera {
                        hdr: true,
                        clear_color: Color::BLACK.into(),
                        ..default()
                    },
                    transform: Transform::from_xyz(0.0, -5.0, 1.0)
                        .looking_at(Vec3::new(0.0, 0.0, 1.0), Vec3::Z),
                    ..default()
                },
                BloomSettings::NATURAL,
                UiCamera,
            ))
            .id();

        commands
            .spawn((SpatialBundle::default(), CameraCradle))
            .add_child(camera);
    }
}

/// System
fn camera_movement(
    mut mouse_motion_events: EventReader<MouseMotion>,
    mut camera_cradle: Query<&mut Transform, With<CameraCradle>>,
    zoom: Res<CameraZoom>,
) {
    const CAMERA_MOVEMENT_SENSITIVITY: f32 = 0.01 / 2.;
    let camera_movement_sensitivity = CAMERA_MOVEMENT_SENSITIVITY / zoom.0.value;

    for event in mouse_motion_events.read() {
        if let Ok(mut camera_cradle) = camera_cradle.get_single_mut() {
            camera_cradle.rotate_z(-event.delta.x * camera_movement_sensitivity);
        }
    }
}

/// System
/// Sets the camera cradle position to the player's position.
/// If you want better movement I suggest to use Freya Holmer's lerp https://youtu.be/LSNQuFEDOyQ
pub fn camera_follow_player(
    player: Query<&Transform, With<MyShip>>,
    mut camera_cradle: Query<&mut Transform, (With<CameraCradle>, Without<MyShip>)>,
) {
    // TODO: Find some way to get_single_mut, but panic when there's more than one player, but do nothing if there's none, cause in some situations we unconditionally need that player, but in other cases we simply need to work on 0 or 1 player.
    if let Ok(player_transform) = player.get_single() {
        if let Ok(mut camera_cradle_transform) = camera_cradle.get_single_mut() {
            camera_cradle_transform.translation = player_transform.translation;
        }
    }
}

#[derive(Resource, Debug)]
pub struct CameraZoom(pub ValueInRange<f32>);

impl Default for CameraZoom {
    fn default() -> Self {
        Self(ValueInRange {
            value: 1.,
            range: (1.)..=(8.),
        })
    }
}

/// System
/// Sets the camera cradle position to the player's position.
/// If you want better movement I suggest to use Freya Holmer's lerp https://youtu.be/LSNQuFEDOyQ
pub fn camera_zoom(
    mut projection: Query<&mut Projection, With<UiCamera>>,
    input: Res<ButtonInput<KeyCode>>,
    mut zoom: ResMut<CameraZoom>,
) {
    const BASE_FOV: f32 = 0.785;

    let Ok(projection) = projection.get_single_mut() else {
        return;
    };

    let Projection::Perspective(perspective) = projection.into_inner() else {
        panic!("Expected Perspective projection on main camera");
    };

    let current_zoom = zoom.0.value;

    if input.just_pressed(KeyCode::KeyR) {
        zoom.0.set(current_zoom * 2.);
    }
    if input.just_pressed(KeyCode::KeyF) {
        zoom.0.set(current_zoom / 2.);
    }

    perspective.fov = BASE_FOV / zoom.0.value;
}

/// System
pub fn update_facing_rotation(
    mut player: Query<&mut FacingRotation, With<MyShip>>,
    camera_cradle: Query<&Transform, (With<CameraCradle>, Without<MyShip>)>,
) {
    // TODO: Find some way to get_single_mut, but panic when there's more than one player, but do nothing if there's none, cause in some situations we unconditionally need that player, but in other cases we simply need to work on 0 or 1 player.
    let Ok(mut facing_rotation) = player.get_single_mut() else {
        return;
    };

    let Ok(camera_cradle_transform) = camera_cradle.get_single() else {
        return;
    };

    facing_rotation.0 =
        camera_cradle_transform.rotation.to_euler(EulerRot::XYZ).2 + std::f32::consts::FRAC_PI_2;
}

fn turn_off_mouse_visibility(window: Query<&mut Window, With<PrimaryWindow>>) {
    set_cursor_visibility(window, false);
}

fn toggle_mouse_visibility(
    window: Query<&mut Window, With<PrimaryWindow>>,
    buttons: Res<ButtonInput<KeyCode>>,
    mut is_visible: Local<bool>,
) {
    if buttons.pressed(KeyCode::ControlLeft) && buttons.just_pressed(KeyCode::KeyE) {
        *is_visible = !*is_visible;

        set_cursor_visibility(window, *is_visible);
    }
}

fn set_cursor_visibility(mut window: Query<&mut Window, With<PrimaryWindow>>, visibility: bool) {
    let Ok(mut window) = window.get_single_mut() else {
        return;
    };
    window.cursor.visible = visibility;
    window.cursor.grab_mode = if visibility {
        CursorGrabMode::None
    } else {
        CursorGrabMode::Locked
    }
}
