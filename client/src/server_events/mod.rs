use bevy::prelude::*;

use bevy_replicon::client::ClientSet;
use hydrogen_line_shared::protocol::events::server::{ReadyToPlay, ShipDestroyed};

use crate::{assets::GenAssets, explosion_effect::spawn_explosion_effect, GameMode};

pub mod chat_message;
use chat_message::*;

pub struct ServerEventsPlugin;

impl Plugin for ServerEventsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            FixedUpdate,
            ((handle_ship_destroyed, handle_chat_message)
                .run_if(in_state(GameMode::MultiplayerBattle)),),
        );

        app.add_systems(
            PreUpdate,
            ((handle_ready_to_play)
                .after(ClientSet::Receive)
                .run_if(in_state(GameMode::MultiplayerBattleLoading)),),
        );
    }
}

fn handle_ship_destroyed(
    mut commands: Commands,
    assets: Res<GenAssets>,
    mut events: EventReader<ShipDestroyed>,
) {
    for ship_destroyed in events.read() {
        spawn_explosion_effect(&mut commands, &assets, *ship_destroyed.position, 1., 15.);
    }
}

fn handle_ready_to_play(
    mut events: EventReader<ReadyToPlay>,
    mut game_mode: ResMut<NextState<GameMode>>,
) {
    for _ in events.read() {
        game_mode.set(GameMode::MultiplayerBattle);
    }
}
