use bevy::prelude::*;
use bevy_replicon::core::ClientId;
use hydrogen_line_shared::protocol::{components::Player, events::server::ChatMessage};

use crate::ui::chat_box::ChatMessageLog;

pub fn handle_chat_message(
    mut events: EventReader<ChatMessage>,
    players: Query<&Player>,
    mut messages: ResMut<ChatMessageLog>,
) {
    for message in events.read() {
        let player = players
            .iter()
            .find(|player| player.client_id == message.from);

        if let Some(player) = player {
            messages
                .0
                .push(format!("[{}]: {}", player.name, message.message));
        } else {
            if message.from == ClientId::SERVER {
                messages.0.push(format!("{}", message.message));
            } else {
                messages
                    .0
                    .push(format!("[id-{:?}]: {}", message.from, message.message));
            }
        }
    }
}
