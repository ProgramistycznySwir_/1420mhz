use bevy::prelude::*;
use hydrogen_line_shared::protocol::events::client::SetupPlayer;

use crate::{CliArgs, GameMode};

pub(super) struct SetupMultiplayerPlugin;

impl Plugin for SetupMultiplayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnEnter(GameMode::MultiplayerBattleLoading),
            (insert_sun, setup_player),
        );
    }
}

fn insert_sun(mut commands: Commands) {
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            shadows_enabled: true,
            ..default()
        },
        transform: Transform {
            rotation: Quat::from_rotation_x(-std::f32::consts::FRAC_PI_4),
            ..default()
        },
        ..default()
    });
}

fn setup_player(cli: Res<CliArgs>, mut setup_player_events: EventWriter<SetupPlayer>) {
    if let Some(name) = &cli.name {
        setup_player_events.send(SetupPlayer {
            name: Some(name.into()),
        });
    } else {
        setup_player_events.send_default();
    }
}
