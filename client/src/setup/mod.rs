use assets::SetupAssetsPlugin;
use bevy::prelude::*;
use multiplayer::SetupMultiplayerPlugin;
use world::SetupWorldPlugin;

pub(crate) mod assets;
mod multiplayer;
mod world;

pub struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            //
            SetupWorldPlugin,
            SetupAssetsPlugin,
            SetupMultiplayerPlugin,
        ));
    }
}
