use bevy::prelude::*;

pub(super) struct SetupWorldPlugin;

impl Plugin for SetupWorldPlugin {
    fn build(&self, app: &mut App) {
        app //
            .insert_resource(ClearColor(Color::BLACK))
            .insert_resource(AmbientLight {
                color: Color::WHITE,
                brightness: 5.0,
            });
    }
}
