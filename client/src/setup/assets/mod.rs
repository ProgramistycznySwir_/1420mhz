use bevy::prelude::*;
use bevy_asset_loader::prelude::*;

use crate::AssetLoadingState;

mod gen;
pub use gen::*;
mod projectiles;
pub use projectiles::*;

pub(super) struct SetupAssetsPlugin;

impl Plugin for SetupAssetsPlugin {
    fn build(&self, app: &mut App) {
        app //
            .add_systems(
                PreStartup,
                (
                    //
                    GenAssets::init,
                    ProjectileAssets::init,
                ),
            )
            .init_state::<AssetLoadingState>()
            .add_loading_state(
                LoadingState::new(AssetLoadingState::Loading)
                    .continue_to_state(AssetLoadingState::Loaded)
                    .load_collection::<ShipModels>()
                    .load_collection::<BasicAssets>(),
            );
    }
}

fn gen_emissive_material(
    materials: &mut Assets<StandardMaterial>,
    base_color: Color,
    intensity: &f32,
) -> Handle<StandardMaterial> {
    materials.add(StandardMaterial {
        base_color,
        emissive: Color::linear_rgb(*intensity, *intensity, *intensity).into(),
        unlit: true,
        ..default()
    })
}

#[derive(Resource, AssetCollection)]
pub struct ShipModels {
    #[asset(path = "models/craft_speederA.glb#Scene0")]
    pub craft_speederA: Handle<Scene>,
    #[asset(path = "models/craft_racer.glb#Scene0")]
    pub craft_racer: Handle<Scene>,
    #[asset(path = "models/craft_miner.glb#Scene0")]
    pub craft_miner: Handle<Scene>,
}

#[derive(Resource, AssetCollection)]
pub struct BasicAssets {
    // #[asset(path = "fonts/FiraSans-Bold.ttf")]
    // pub font_firaSans: Handle<Font>
}
