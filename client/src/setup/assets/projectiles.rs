use bevy::{color::palettes::css, prelude::*};

use super::gen_emissive_material;

/// Assets that are generated for game.
///
/// Don't forget to initialize it by calling `Self::init()`
#[derive(Resource)]
pub struct ProjectileAssets {
    pub projectile_120mm_mesh: Handle<Mesh>,
    pub projectile_20mm_mesh: Handle<Mesh>,
    pub projectile_120mm_material: Handle<StandardMaterial>,
    pub projectile_20mm_material: Handle<StandardMaterial>,
}

impl ProjectileAssets {
    /// Startup system
    pub fn init(
        mut commands: Commands,
        _server: Res<AssetServer>,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        commands.insert_resource(Self {
            projectile_120mm_mesh: meshes.add(Mesh::from(Capsule3d::new(0.5, 6.))),
            projectile_20mm_mesh: meshes.add(Mesh::from(Capsule3d::new(0.2, 3.))),
            projectile_120mm_material: gen_emissive_material(&mut materials, css::RED.into(), &20.),
            projectile_20mm_material: gen_emissive_material(
                &mut materials,
                css::YELLOW.into(),
                &15.,
            ),
        });
    }
}
