use bevy::{color::palettes::css, prelude::*};

use super::gen_emissive_material;

/// Assets that are generated for game.
///
/// Don't forget to initialize it by calling `Self::init()`
#[derive(Resource)]
pub struct GenAssets {
    pub cube_mesh: Handle<Mesh>,
    pub sphere_mesh: Handle<Mesh>,
    pub orange_red_material: Handle<StandardMaterial>,
    pub orange_red_material_emissive_3: Handle<StandardMaterial>,
    pub white_material: Handle<StandardMaterial>,
    pub white_material_emissive_1: Handle<StandardMaterial>,
    pub white_material_emissive_3: Handle<StandardMaterial>,
    pub white_material_emissive_5: Handle<StandardMaterial>,
    pub white_material_transparent_20: Handle<StandardMaterial>,
}

impl GenAssets {
    /// Startup system
    pub fn init(
        mut commands: Commands,
        _server: Res<AssetServer>,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        commands.insert_resource(Self {
            cube_mesh: meshes.add(Mesh::from(Cuboid::new(1., 1., 1.))),
            sphere_mesh: meshes.add(Mesh::from(Sphere::new(0.5))),
            orange_red_material: materials.add(StandardMaterial::from_color(css::ORANGE_RED)),
            orange_red_material_emissive_3: gen_emissive_material(
                &mut materials,
                css::ORANGE_RED.into(),
                &3.,
            ),
            white_material: materials.add(Color::WHITE),
            white_material_emissive_1: gen_emissive_material(&mut materials, Color::WHITE, &1.),
            white_material_emissive_3: gen_emissive_material(&mut materials, Color::WHITE, &3.),
            white_material_emissive_5: gen_emissive_material(&mut materials, Color::WHITE, &5.),
            white_material_transparent_20: materials.add(StandardMaterial {
                base_color: Color::WHITE.with_alpha(0.2),
                alpha_mode: AlphaMode::Blend,
                double_sided: true,
                ..default()
            }),
        });
    }
}
