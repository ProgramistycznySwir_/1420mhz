use std::net::{IpAddr, Ipv4Addr};

use bevy::prelude::*;
use clap::Parser;
use hydrogen_line_shared::protocol::components::NetworkVelocity;

use crate::camera_state::UiCamera;

pub mod assets {
    pub use super::setup::assets::*;
}

mod app;
mod camera;
mod camera_state;
mod explosion_effect;
mod input;
mod misc;
mod networking;
mod player_gizmo;
mod replication;
mod server_events;
mod setup;
mod ui;

fn main() {
    app::app();
}

#[derive(Parser, PartialEq, Resource)]
struct CliArgs {
    #[arg(short, long, default_value_t = Ipv4Addr::LOCALHOST.into())]
    ip: IpAddr,
    #[arg(short, long)]
    name: Option<String>,
}
impl Default for CliArgs {
    fn default() -> Self {
        Self::parse()
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
enum AssetLoadingState {
    #[default]
    Loading,
    Loaded,
}

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
enum GameMode {
    StartMenu,
    Hangar,
    #[default]
    /// Connecting to server
    MultiplayerBattleConnecting,
    /// Loading game data from server
    MultiplayerBattleLoading,
    /// Main game loop
    MultiplayerBattle,
}

impl GameMode {
    /// ## Running condition.
    pub fn in_multiplayer_battle(game_mode: Res<State<GameMode>>) -> bool {
        *game_mode == GameMode::MultiplayerBattleLoading
            || *game_mode == GameMode::MultiplayerBattle
    }
}

fn projectile_update(time: Res<Time>, mut projectiles: Query<(&mut Transform, &NetworkVelocity)>) {
    projectiles
        .par_iter_mut()
        .for_each(|(mut transform, velocity)| {
            transform.translation += velocity.extend(0.) * time.delta_seconds();
        });
}

/// Singleton
/// Indicates the client's own entity
#[derive(Component, Default, Debug, Clone, Copy)]
struct MyShip;
