use bevy::prelude::*;

use crate::GameMode;

/// Marker Component - used to mark TargetCamera for UI
#[derive(Component, PartialEq)]
pub struct UiCamera;

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
pub enum BattleCameraState {
    #[default]
    NotPresent,
    Present,
}

pub struct CameraStateManagerPlugin;

impl Plugin for CameraStateManagerPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<BattleCameraState>().add_systems(
            Update,
            (|mut next_state: ResMut<NextState<BattleCameraState>>,
              camera_query: Query<(Entity, &UiCamera)>| {
                next_state.set(if camera_query.iter().count() > 0 {
                    BattleCameraState::Present
                } else {
                    BattleCameraState::NotPresent
                });
            })
            .run_if(in_state(GameMode::MultiplayerBattle)),
        );
    }
}
