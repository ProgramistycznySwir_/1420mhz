use std::time::SystemTime;

use bevy::{prelude::*, render::view::screenshot::ScreenshotManager, window::PrimaryWindow};

pub struct ScreenshotPlugin;

impl Plugin for ScreenshotPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, take_screenshot);
    }
}

fn take_screenshot(
    input: Res<ButtonInput<KeyCode>>,
    main_window: Query<Entity, With<PrimaryWindow>>,
    mut screenshot_manager: ResMut<ScreenshotManager>,
) {
    if input.just_pressed(KeyCode::F12) {
        let current_time = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap();
        let path = format!("./screenshot-{}.png", current_time.as_millis());
        screenshot_manager
            .save_screenshot_to_disk(main_window.single(), path)
            .expect("Screenshot system tried to save to disk");
    }
}
