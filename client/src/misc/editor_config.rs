use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::prelude::*;
use bevy_editor_pls::controls;
use bevy_editor_pls::controls::EditorControls;
// use bevy_editor_pls::EditorPlugin;
// use bevy_editor_pls_default_windows::hierarchy::picking::EditorRayCastSource;

pub struct EditorPlugin;

impl Plugin for EditorPlugin {
    fn build(&self, app: &mut App) {
        app //
            .add_plugins((
                bevy_editor_pls::EditorPlugin::default(),
                FrameTimeDiagnosticsPlugin,
            ))
            .insert_resource(editor_controls());
    }
}

fn editor_controls() -> EditorControls {
    let mut editor_controls = EditorControls::default_bindings();
    editor_controls.unbind(controls::Action::PlayPauseEditor);

    editor_controls.insert(
        controls::Action::PlayPauseEditor,
        controls::Binding {
            input: controls::UserInput::Chord(vec![
                controls::Button::Keyboard(KeyCode::ControlLeft),
                controls::Button::Keyboard(KeyCode::KeyE),
            ]),
            conditions: vec![controls::BindingCondition::ListeningForText(false)],
        },
    );

    editor_controls
}
