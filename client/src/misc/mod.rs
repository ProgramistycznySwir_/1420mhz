use bevy::prelude::*;

use editor_config::EditorPlugin;
use preserve::PreservePlugin;
use screenshot::ScreenshotPlugin;

mod editor_config;
pub mod preserve;
mod screenshot;

pub struct MiscPlugin;

impl Plugin for MiscPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            //
            ScreenshotPlugin,
            PreservePlugin,
        ));

        #[cfg(debug_assertions)]
        {
            app.add_plugins((
                //
                EditorPlugin,
            ));
        }
    }
}
