use std::marker::PhantomData;

use bevy::{prelude::*, utils::HashMap};

pub struct PreservePlugin;

impl Plugin for PreservePlugin {
    fn build(&self, app: &mut App) {
        preserve::<Transform>(app);
    }
}

fn preserve<T: Component + Clone>(app: &mut App) {
    app //
        .init_resource::<Preserved<T>>()
        .add_systems(First, preserve_update::<T>)
        .add_systems(Last, preserve_cleanup::<T>);
}

/// Preservers component data in the frame the entity was destroyed.
#[derive(Component, Reflect, Default, Debug)]
pub struct Preserve<T: Component + Clone>(PhantomData<T>);

/// Preservers component data in the frame the entity was destroyed.
#[derive(Resource, Reflect, Debug)]
pub struct Preserved<T: Component + Clone>(pub HashMap<Entity, T>);

impl<T: Component + Clone> Default for Preserved<T> {
    fn default() -> Self {
        Self(Default::default())
    }
}

fn preserve_update<T: Component + Clone>(
    mut preserved: ResMut<Preserved<T>>,
    query: Query<(Entity, &T), With<Preserve<T>>>,
) {
    for component in query.iter() {
        preserved.0.insert(component.0, component.1.clone());
    }
}

fn preserve_cleanup<T: Component + Clone>(
    mut preserved: ResMut<Preserved<T>>,
    mut removed: RemovedComponents<T>,
) {
    for entity in removed.read() {
        preserved.0.remove(&entity);
    }
}
