use bevy::prelude::*;
use bevy_replicon::client::ClientSet;
use hydrogen_line_shared::protocol::components::Projectile;

use crate::{
    assets::GenAssets, explosion_effect::spawn_explosion_effect, misc::preserve::Preserved,
    GameMode,
};

/// Groups up all logic for terminating replicated entities.
pub(super) struct TermsPlugin;

impl Plugin for TermsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PreUpdate,
            (
                //
                term_projectiles,
            )
                .after(ClientSet::Receive)
                .run_if(GameMode::in_multiplayer_battle),
        );
    }
}

// TODO LOW: Those effects sometimes look weird cause projectiles position is interpolated on the client sometimes is accurate, sometimes one frame late, it would be probably best to send those positions from the server in events, but it may negatively impact netcode performance.
fn term_projectiles(
    mut commands: Commands,
    mut removed_projectiles: RemovedComponents<Projectile>,
    projectiles_transforms: Res<Preserved<Transform>>,
    assets: Res<GenAssets>,
) {
    for projectile in removed_projectiles.read() {
        let Some(transform) = projectiles_transforms.0.get(&projectile) else {
            info!("Projectile not found");
            continue;
        };

        spawn_explosion_effect(&mut commands, &assets, transform.translation.xy(), 0.5, 0.5);
    }
}
