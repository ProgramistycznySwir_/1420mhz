use crate::GameMode;
use bevy::prelude::*;
use bevy_replicon::client::ClientSet;
use hydrogen_line_shared::protocol::components::transforms::NetworkTransform;
use inits::InitsPlugin;
use terms::TermsPlugin;

pub mod inits;
pub mod terms;

/// Groups up all client-side replication logic
pub struct ReplicationPlugin;

impl Plugin for ReplicationPlugin {
    fn build(&self, app: &mut App) {
        app //
            .add_plugins((
                //
                InitsPlugin,
                TermsPlugin,
            ))
            .add_systems(
                PreUpdate,
                (sync_transforms)
                    .after(ClientSet::Receive)
                    .run_if(in_state(GameMode::MultiplayerBattle)),
            );
    }
}

fn sync_transforms(
    mut query: Query<(&mut Transform, &NetworkTransform), Changed<NetworkTransform>>,
) {
    for (mut transform, network_transform) in &mut query {
        transform.translation = network_transform.position.0.extend(0.0);
        transform.rotation = Quat::from_rotation_z(*network_transform.rotation);
    }
}
