use bevy::prelude::*;
use bevy_replicon::client::ClientSet;
use hydrogen_line_shared::{
    protocol::components::{
        transforms::{InitialNetworkTransform, NetworkScale, NetworkTransform},
        weapons::{NetworkProjectileType, NetworkTurret},
        Asteroid, Player, Projectile,
    },
    FacingRotation,
};

use crate::{
    assets::{GenAssets, ProjectileAssets, ShipModels},
    misc::preserve::Preserve,
    networking::MyClientId,
    GameMode, MyShip,
};

/// Groups up all logic for initializing replicated entities.
pub(super) struct InitsPlugin;

impl Plugin for InitsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PreUpdate,
            (
                //
                init_players,
                init_turrets,
                init_asteroids,
                init_projectiles,
            )
                .after(ClientSet::Receive)
                .run_if(GameMode::in_multiplayer_battle),
        );
    }
}

pub fn init_players(
    mut commands: Commands,
    spawned_players: Query<(&Player, Entity, &NetworkTransform), Added<Player>>,
    my_client_id: Res<MyClientId>,
    assets: Res<AssetServer>,
    ship_assets: Res<ShipModels>,
) {
    let ROTATION: Quat = Quat::from_euler(EulerRot::XYZ, std::f32::consts::PI * 0.5, 0.0, 0.0);
    for (player, player_entity, transform) in &spawned_players {
        commands
            .entity(player_entity)
            .insert(SpatialBundle::from_transform(Into::<Transform>::into(
                transform,
            )));

        let ship = commands
            .spawn(SceneBundle {
                scene: ship_assets.craft_speederA.clone(),
                transform: Transform::from_xyz(-2., 1.5, -0.25).with_rotation(ROTATION),
                ..default()
            })
            .id();

        commands.entity(player_entity).add_child(ship);

        if player.client_id == my_client_id.0 {
            commands
                .entity(player_entity)
                .insert((MyShip, FacingRotation::default()));
        }
    }
}

fn init_turrets(
    mut commands: Commands,
    turrets: Query<(Entity, &InitialNetworkTransform), Added<NetworkTurret>>,
    assets: Res<AssetServer>,
) {
    for (entity, transform) in &turrets {
        commands
            .entity(entity)
            .insert(SpatialBundle::from_transform(Into::<Transform>::into(
                &transform.0,
            )));
    }
}

fn init_asteroids(
    mut commands: Commands,
    spawned_asteroids: Query<(Entity, &NetworkTransform, &NetworkScale), Added<Asteroid>>,
    gen_assets: Res<GenAssets>,
) {
    for (entity, network_transform, network_scale) in &spawned_asteroids {
        commands.entity(entity).insert(PbrBundle {
            mesh: gen_assets.cube_mesh.clone(),
            transform: Transform::from_translation(network_transform.position.extend(0.))
                .with_rotation(Quat::from_euler(
                    EulerRot::XYZ,
                    0.,
                    0.,
                    *network_transform.rotation,
                ))
                .with_scale(network_scale.0),
            material: gen_assets.white_material.clone(),
            ..default()
        });
    }
}

fn init_projectiles(
    mut commands: Commands,
    spawned_projectiles: Query<
        (Entity, &InitialNetworkTransform, &NetworkProjectileType),
        Added<Projectile>,
    >,
    assets: Res<ProjectileAssets>,
) {
    for (entity, network_transform, projectile_type) in &spawned_projectiles {
        match projectile_type {
            NetworkProjectileType::Kinetic120mm => {
                commands
                    .entity(entity)
                    .insert(PbrBundle {
                        mesh: assets.projectile_120mm_mesh.clone(),
                        transform: Transform::from_translation(
                            network_transform.position.extend(0.),
                        )
                        .with_rotation(Quat::from_euler(
                            EulerRot::XYZ,
                            0.,
                            0.,
                            *network_transform.rotation,
                        )),
                        // .with_scale(Vec3::new(0.5, 6., 0.5)),
                        material: assets.projectile_120mm_material.clone(),
                        ..default()
                    })
                    .insert(Preserve::<Transform>::default());
            }
            NetworkProjectileType::Kinetic20mm => {
                commands
                    .entity(entity)
                    .insert(PbrBundle {
                        mesh: assets.projectile_20mm_mesh.clone(),
                        transform: Transform::from_translation(
                            network_transform.position.extend(0.),
                        )
                        .with_rotation(Quat::from_euler(
                            EulerRot::XYZ,
                            0.,
                            0.,
                            *network_transform.rotation,
                        )),
                        // .with_scale(Vec3::new(0.2, 3., 0.2)),
                        material: assets.projectile_20mm_material.clone(),
                        ..default()
                    })
                    .insert(Preserve::<Transform>::default());
            }
        }
    }
}
