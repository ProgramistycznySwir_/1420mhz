use crate::camera_state::CameraStateManagerPlugin;
use crate::explosion_effect::ExplosionEffectPlugin;
use crate::input::InputPlugin;
use crate::misc::MiscPlugin;
use crate::setup::SetupPlugin;
use crate::ui::UiPlugin;
use crate::{camera::CameraPlugin, GameMode};
use bevy::{
    prelude::*,
    winit::{UpdateMode::Continuous, WinitSettings},
};
use bevy_easings::EasingsPlugin;
use bevy_mod_bbcode::BbcodePlugin;
use bevy_replicon::prelude::*;
use bevy_replicon_renet::RepliconRenetPlugins;
use hydrogen_line_shared::protocol::ProtocolPlugin;

use crate::player_gizmo::PlayerGizmoPlugin;
use crate::replication::ReplicationPlugin;
use crate::server_events::ServerEventsPlugin;
use crate::{networking::*, projectile_update, CliArgs};

pub(super) fn app() {
    App::new()
        // Libs
        .add_plugins((DefaultPlugins, RepliconPlugins, RepliconRenetPlugins))
        .add_plugins(EasingsPlugin)
        .insert_resource(WinitSettings {
            focused_mode: Continuous,
            unfocused_mode: Continuous,
        })
        .add_plugins(BbcodePlugin::new().with_fonts("fonts"))
        // Resources
        .init_resource::<CliArgs>()
        .init_state::<GameMode>()
        // Miscelaneous plugins (without clear category)
        .add_plugins(MiscPlugin)
        // Game setup
        .add_plugins((SetupPlugin, CameraStateManagerPlugin))
        // Networking
        .add_plugins((NetworkingPlugin, ProtocolPlugin))
        .add_plugins((ServerEventsPlugin, ReplicationPlugin))
        // Other
        .add_plugins((
            //
            UiPlugin,
            InputPlugin,
            CameraPlugin,
            PlayerGizmoPlugin,
            ExplosionEffectPlugin,
        ))
        // Probably not moved to a separate module
        .add_systems(
            Update,
            projectile_update.run_if(in_state(GameMode::MultiplayerBattle)),
        )
        .run();
}
