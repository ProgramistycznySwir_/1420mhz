use std::f32::consts::FRAC_PI_2;

use bevy::{input::keyboard::KeyboardInput, prelude::*};
use hydrogen_line_shared::{
    protocol::events::client::{player_movement::PlayerMovement, FireWeaponGroup},
    FacingRotation,
};

use crate::{camera::CameraCradle, GameMode};

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                // update_cursor_state_from_window
                //     .run_if(run_if_enabled::<BoxMovement>)
                //     .in_set(InputManagerSystem::ManualControl)
                //     .before(InputManagerSystem::ReleaseOnDisable)
                //     .after(InputManagerSystem::Tick)
                //     .after(InputManagerSystem::Update)
                //     .after(InputSystem),
                movement_input,
                mouse_button_input,
                keyboard_input,
            )
                .run_if(in_state(GameMode::MultiplayerBattle))
                .chain(),
        );
    }
}

/// Reads player inputs and sends [`PlayerMovement`] events.
fn movement_input(
    mut move_events: EventWriter<PlayerMovement>,
    input: Res<ButtonInput<KeyCode>>,
    facing_rotation: Query<&FacingRotation>,
) {
    let Ok(facing_rotation) = facing_rotation.get_single() else {
        return;
    };

    let mut throttle_x = 0.0;
    let mut throttle_y = 0.0;
    let mut rotation = 0.0;

    if input.pressed(KeyCode::KeyE) {
        throttle_x += 1.0;
    }
    if input.pressed(KeyCode::KeyQ) {
        throttle_x -= 1.0;
    }
    if input.pressed(KeyCode::KeyW) {
        throttle_y += 1.0;
    }
    if input.pressed(KeyCode::KeyS) {
        throttle_y -= 1.0;
    }
    if input.pressed(KeyCode::KeyA) {
        rotation += 1.0;
    }
    if input.pressed(KeyCode::KeyD) {
        rotation -= 1.0;
    }

    move_events.send(PlayerMovement {
        throttle_x,
        throttle_y,
        rotation,
        facing_rotation: facing_rotation.0,
    });
}

fn mouse_button_input(
    mut fire_events: EventWriter<FireWeaponGroup>,
    buttons: Res<ButtonInput<MouseButton>>,
) {
    // TODO LOW: In future make weapon groups bindable to other inputs.
    if buttons.pressed(MouseButton::Left) {
        fire_events.send(FireWeaponGroup::new(0));
    }
    if buttons.pressed(MouseButton::Right) {
        fire_events.send(FireWeaponGroup::new(1));
    }
}

fn keyboard_input(
    mut fire_events: EventWriter<FireWeaponGroup>,
    buttons: Res<ButtonInput<KeyCode>>,
) {
    if buttons.pressed(KeyCode::Space) {
        fire_events.send(FireWeaponGroup::new(2));
    }
}
