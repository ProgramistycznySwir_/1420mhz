use std::time::Duration;

use bevy::prelude::*;
use bevy_easings::*;

use crate::assets::GenAssets;

pub struct ExplosionEffectPlugin;

impl Plugin for ExplosionEffectPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Update, destroy_when_elapsed_system);
    }
}

#[derive(Component)]
struct DestroyWhenElapsed(Timer);

// #[derive(Component)]
// struct ExplosionEffect {
//     lifetime: DestroyWhenElapsed,
//     max_radius: f32,
// }

pub fn spawn_explosion_effect(
    commands: &mut Commands,
    assets: &Res<GenAssets>,
    position: Vec2,
    time: f32,
    radius: f32,
) {
    let inner_sphere = commands
        .spawn(PbrBundle {
            transform: Transform::from_scale(Vec3::splat(0.8)),
            mesh: assets.sphere_mesh.clone(),
            material: assets.orange_red_material_emissive_3.clone(),
            ..default()
        })
        .id();
    let outer_sphere = commands
        .spawn(PbrBundle {
            transform: Transform::default(),
            mesh: assets.sphere_mesh.clone(),
            material: assets.white_material_transparent_20.clone(),
            ..default()
        })
        .id();

    // let light = commands
    //     .spawn(PointLightBundle {
    //         point_light: PointLight {
    //             intensity: 50.,
    //             range: 200.,
    //             ..default()
    //         },
    //         ..default()
    //     })
    //     .id();

    commands
        .spawn((
            SpatialBundle::from_transform(
                Transform::from_translation(position.extend(0.)), //.with_scale(Vec3::splat(100.)),
            ),
            Transform::from_translation(position.extend(0.)).ease_to(
                Transform::from_translation(position.extend(0.)).with_scale(Vec3::splat(radius)),
                EaseFunction::CubicIn,
                EasingType::Once {
                    duration: Duration::from_secs_f32(time / 2.),
                },
            ),
            DestroyWhenElapsed(Timer::new(Duration::from_secs_f32(time), TimerMode::Once)),
        ))
        .push_children(&[inner_sphere, outer_sphere]);
}

// fn explosion_effect_update(query: Query<(&Transform, &mut DestroyWhenElapsed)>) {}
fn destroy_when_elapsed_system(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &mut DestroyWhenElapsed)>,
) {
    query.iter_mut().for_each(|mut entity| {
        if entity.1 .0.finished() {
            commands.entity(entity.0).despawn_recursive();
        }
        entity.1 .0.tick(time.delta());
    });
}
