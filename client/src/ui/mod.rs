use bevy::prelude::*;
use chat_box::ChatBoxPlugin;
use debug_network_info::DebugNetworkInfoPlugin;
use position_text::PositionTextPlugin;

pub mod chat_box;
pub mod debug_network_info;
pub mod position_text;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            //
            ChatBoxPlugin,
            PositionTextPlugin,
        ));

        #[cfg(debug_assertions)]
        {
            app.add_plugins((
                //
                DebugNetworkInfoPlugin,
            ));
        }
    }
}
