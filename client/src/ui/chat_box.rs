use crate::{
    camera_state::{BattleCameraState, UiCamera},
    server_events::chat_message::handle_chat_message,
};
use bevy::prelude::*;
use bevy_mod_bbcode::{Bbcode, BbcodeBundle, BbcodePlugin, BbcodeSettings};

pub(super) struct ChatBoxPlugin;

impl Plugin for ChatBoxPlugin {
    fn build(&self, app: &mut App) {
        app //
            .init_resource::<ChatMessageLog>()
            .add_systems(OnEnter(BattleCameraState::Present), setup)
            .add_systems(
                Update,
                (update)
                    .after(handle_chat_message)
                    .run_if(in_state(BattleCameraState::Present)),
            );

        #[cfg(debug_assertions)]
        {
            app.register_type::<ChatMessageLog>();
        }
    }
}

#[derive(Resource, Reflect, Default, Debug)]
pub struct ChatMessageLog(pub Vec<String>);

const ID: &str = "chat_box";
const CHAT_BOX_COLOR: Color = Color::srgba(0., 0., 0., 0.5);

fn setup(mut commands: Commands, main_camera: Query<(Entity, &UiCamera)>) {
    commands.spawn((
        TargetCamera(main_camera.single().0),
        Name::new(ID),
        NodeBundle {
            style: Style {
                position_type: PositionType::Absolute,

                // left: Val::Px(10.),
                // bottom: Val::Px(10.),
                justify_self: JustifySelf::Center,
                top: Val::Px(60.),

                // flex_direction: FlexDirection::Column,
                ..default()
            },
            background_color: BackgroundColor(CHAT_BOX_COLOR),
            ..default()
        },
        // TextBundle::from_section(
        //     "",
        //     TextStyle {
        //         font_size: 16.0,
        //         color: Color::WHITE,
        //         ..default()
        //     },
        // )
    ));
}

fn update(
    mut commands: Commands,
    chat_box: Query<(&Name, Entity, Option<&Children>)>,
    messages: Res<ChatMessageLog>,
) {
    if !messages.is_changed() {
        return;
    }

    let (_, chat_box, chat_box_messages) = chat_box
        .iter()
        .find(|(name, _, _)| name.eq_ignore_ascii_case(ID))
        .expect("There should be a chat box entity present");

    let last_message = messages
        .0
        .iter()
        .last()
        .expect("There should be at least one message in log at this point");

    commands.entity(chat_box).with_children(|parent| {
        parent.spawn(BbcodeBundle::from_content(
            last_message,
            BbcodeSettings::new("Fira Sans", 16., Color::WHITE),
        ));
    });

    if let Some(chat_box_messages) = chat_box_messages {
        if chat_box_messages.len() >= 5 {
            commands
                .entity(chat_box)
                .remove_children(&[chat_box_messages.first().unwrap().to_owned()]);
        }
    }

    info!("{:?}", messages);

    // commands.entity(chat_box).chil

    // let messages: Vec<String> = messages.0.iter().cloned().rev().take(6).rev().collect();

    // chat_box.sections[0].value = messages.join(&"\n");
}
