use bevy::prelude::*;
use bevy_replicon_renet::renet::{transport::NetcodeClientTransport, RenetClient};

use crate::camera_state::{BattleCameraState, UiCamera};

pub(super) struct DebugNetworkInfoPlugin;

impl Plugin for DebugNetworkInfoPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(BattleCameraState::Present), setup)
            .add_systems(
                Update,
                (update).run_if(in_state(BattleCameraState::Present)),
            );
    }
}

const ID: &str = "DEBUG_networking_info_text";

fn setup(mut commands: Commands, main_camera: Query<(Entity, &UiCamera)>) {
    commands.spawn((
        TargetCamera(main_camera.single().0),
        TextBundle::from_section(
            "",
            TextStyle {
                font_size: 16.0,
                color: Color::WHITE,
                ..default()
            },
        )
        .with_style(Style { ..default() })
        .with_background_color(Color::rgba(0., 0., 0., 0.5)),
        Name::new(ID),
    ));

    info!("DEBUG_networking_info_setup");
}

fn update(
    mut query: Query<(&Name, &mut Text)>,
    client: Res<RenetClient>,
    transport: Res<NetcodeClientTransport>,
) {
    let client_id = transport.client_id();
    let down = {
        let bandwidth = client.bytes_received_per_sec();
        if bandwidth > 1_000_000. {
            let bandwidth = bandwidth / 1_000_000.;
            format!("{bandwidth:.2}MB/s")
        } else if bandwidth > 1_000. {
            let bandwidth = bandwidth / 1_000.;
            format!("{bandwidth:.2}kB/s")
        } else {
            format!("{bandwidth:.0}B/s")
        }
    };
    let up = {
        let bandwidth = client.bytes_sent_per_sec();
        if bandwidth > 1_000_000. {
            let bandwidth = bandwidth / 1_000_000.;
            format!("{bandwidth:.2}MB/s")
        } else if bandwidth > 1_000. {
            let bandwidth = bandwidth / 1_000.;
            format!("{bandwidth:.2}kB/s")
        } else {
            format!("{bandwidth:.0}B/s")
        }
    };
    let pl = client.packet_loss() * 100.0;
    let rtt = client.rtt() * 1000.0;

    query
        .iter_mut()
        .find(|(name, _)| name.eq_ignore_ascii_case(ID))
        .unwrap()
        .1
        .sections[0]
        .value =
        format!("client_id: {client_id}\ndn: {down}\nup: {up}\npl: {pl:.1}%\nrtt: {rtt:.1}ms")
            .into();
}
