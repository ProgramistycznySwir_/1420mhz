use crate::{
    camera_state::{BattleCameraState, UiCamera},
    networking::MyClientId,
};
use bevy::prelude::*;
use hydrogen_line_shared::protocol::components::{transforms::NetworkTransform, Player};

pub(super) struct PositionTextPlugin;

impl Plugin for PositionTextPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(BattleCameraState::Present), setup)
            .add_systems(
                Update,
                (update).run_if(in_state(BattleCameraState::Present)),
            );
    }
}

const PLAYER_POSITION_TEXT_NAME: &str = "position_text";

fn update(
    mut query: Query<(&Name, &mut Text)>,
    player_position: Query<(&Player, &NetworkTransform)>,
    my_client_id: Res<MyClientId>,
) {
    let player_position = player_position
        .iter()
        .find(|(player, _)| player.client_id == my_client_id.0);

    if let Some((_, player_transform)) = player_position {
        query
            .iter_mut()
            .find(|(name, _)| name.eq_ignore_ascii_case(PLAYER_POSITION_TEXT_NAME))
            .unwrap()
            .1
            .sections[0]
            .value = format!(
            "{:.0} | {:.0}",
            player_transform.position.0.x, player_transform.position.0.y
        )
        .into();
    }
}

fn setup(mut commands: Commands, main_camera: Query<(Entity, &UiCamera)>) {
    commands.spawn((
        TargetCamera(main_camera.single().0),
        TextBundle::from_section(
            "",
            TextStyle {
                font_size: 16.0,
                color: Color::WHITE,
                ..default()
            },
        )
        .with_style(Style {
            position_type: PositionType::Absolute,
            top: Val::Px(30.),
            justify_self: JustifySelf::Center,
            ..default()
        }),
        Name::new(PLAYER_POSITION_TEXT_NAME),
    ));
}
