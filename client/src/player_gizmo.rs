use std::f32::consts::FRAC_PI_2;

use bevy::{color::palettes::css, prelude::*};
use hydrogen_line_shared::{
    protocol::components::{
        transforms::{NetworkRotation, NetworkTransform},
        weapons::NetworkTurret,
        NetworkDurability, Player,
    },
    FacingRotation,
};

use crate::{camera::camera_follow_player, networking::MyClientId, GameMode, MyShip};

pub struct PlayerGizmoPlugin;

impl Plugin for PlayerGizmoPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(
            PostUpdate,
            (
                draw_player_collider_gizmos,
                draw_nearby_player_gizmos,
                draw_facing_direction_gizmo.after(camera_follow_player),
                draw_turrets_gizmos.after(TransformSystem::TransformPropagate),
            )
                .run_if(in_state(GameMode::MultiplayerBattle)),
        );
    }
}

const PLAYER_COLLIDER_RADIUS: f32 = 2.;

fn draw_player_collider_gizmos(
    mut gizmos: Gizmos,
    my_client_id: Res<MyClientId>,
    players: Query<(&Player, &NetworkTransform, &NetworkDurability)>,
) {
    for (player, network_transform, durability) in &players {
        let (color, direction_angle) = if player.client_id == my_client_id.0 {
            (
                Color::srgba(0.5, 0.5, 1., 0.5),
                -network_transform.rotation.0,
            )
        } else {
            (Color::srgba(1., 0., 0., 0.5), network_transform.rotation.0)
        };

        gizmos.arc_2d(
            *network_transform.position,
            direction_angle,
            std::f32::consts::TAU * durability.0.percent(),
            PLAYER_COLLIDER_RADIUS,
            color,
        );
    }
}

fn draw_nearby_player_gizmos(
    mut gizmos: Gizmos,
    my_client_id: Res<MyClientId>,
    players: Query<(&Player, &NetworkTransform)>,
) {
    let me = players
        .iter()
        .find(|player| player.0.client_id == my_client_id.0);

    if let Some(me) = me {
        for (_, network_transform) in players
            .iter()
            .filter(|player| player.0.client_id != my_client_id.0)
        {
            gizmos.arc_2d(
                me.1.position.0,
                Vec2::Y.angle_between(network_transform.position.0 - me.1.position.0),
                std::f32::consts::FRAC_PI_4,
                PLAYER_COLLIDER_RADIUS + 0.5,
                css::RED,
            );
        }
    }
}

fn draw_facing_direction_gizmo(
    mut gizmos: Gizmos,
    my_ship: Query<(&Transform, &FacingRotation), With<MyShip>>,
) {
    let Ok((my_ship_transform, player_facing_rotation)) = my_ship.get_single() else {
        return;
    };

    const LENGTH: f32 = 100.;

    let line_end = Vec2::from_angle(player_facing_rotation.0) * LENGTH;

    gizmos.line_2d(
        my_ship_transform.translation.xy(),
        my_ship_transform.translation.xy() + line_end,
        css::GRAY,
    );
}

fn draw_turrets_gizmos(
    mut gizmos: Gizmos,
    my_ship: Query<(&Transform, &Children, &FacingRotation), With<MyShip>>,
    turrets: Query<(&GlobalTransform, &NetworkRotation, &NetworkTurret)>,
) {
    let Ok((my_ship_transform, children, player_facing_rotation)) = my_ship.get_single() else {
        return;
    };

    for child in children.iter() {
        let Ok((turret_transform, rotation, turret)) = turrets.get(*child) else {
            continue;
        };

        const LENGTH: f32 = 100.;
        let turret_facing_rotation =
            my_ship_transform.rotation.to_euler(EulerRot::XYZ).2 + rotation.0 + FRAC_PI_2;

        let line_end = Vec2::from_angle(turret_facing_rotation) * LENGTH;

        let desired_vs_actual_rotation_delta =
            (player_facing_rotation.0 - turret_facing_rotation).abs();

        gizmos.line_2d(
            turret_transform.translation().xy(),
            turret_transform.translation().xy() + line_end,
            if desired_vs_actual_rotation_delta < turret.aiming_tolerance {
                css::GREEN
            } else {
                css::RED
            },
        );
    }
}
