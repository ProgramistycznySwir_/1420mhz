use std::{
    error::Error,
    net::{IpAddr, Ipv4Addr, SocketAddr, UdpSocket},
    time::SystemTime,
};

use bevy::prelude::*;
use bevy_replicon::prelude::*;
use bevy_replicon_renet::{
    renet::{
        transport::{ClientAuthentication, NetcodeClientTransport},
        ConnectionConfig, RenetClient,
    },
    RenetChannelsExt,
};

use crate::{CliArgs, GameMode};

const PORT: u16 = 1337;
const PROTOCOL_ID: u64 = 0;

pub struct NetworkingPlugin;

impl Plugin for NetworkingPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnEnter(GameMode::MultiplayerBattleConnecting),
            setup_networking.map(Result::unwrap),
        );

        app.add_systems(
            Update,
            (handle_client_connected).run_if(client_just_connected),
        );
    }
}

#[derive(Resource)]
pub struct MyClientId(pub ClientId);

pub fn setup_networking(
    mut commands: Commands,
    channels: Res<RepliconChannels>,
    cli: Res<CliArgs>,
) -> Result<(), Box<dyn Error>> {
    let server_channels_config = channels.get_server_configs();
    let client_channels_config = channels.get_client_configs();

    let client = RenetClient::new(ConnectionConfig {
        server_channels_config,
        client_channels_config,
        ..Default::default()
    });

    let current_time = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?;
    // TODO: Should be made random to avoid collisions
    let client_id = (current_time.as_nanos() % u64::MAX as u128) as u64;
    let server_addr = SocketAddr::new(cli.ip, PORT);
    let socket_ip: IpAddr = Ipv4Addr::UNSPECIFIED.into();
    let socket = UdpSocket::bind((socket_ip, 0))?;
    let authentication = ClientAuthentication::Unsecure {
        client_id,
        protocol_id: PROTOCOL_ID,
        server_addr,
        user_data: None,
    };
    let transport = NetcodeClientTransport::new(current_time, authentication, socket)?;

    commands.insert_resource(client);
    commands.insert_resource(transport);
    commands.insert_resource(MyClientId(ClientId::new(client_id)));

    info!("Client ready!");

    Ok(())
}

fn handle_client_connected(mut next_game_mode: ResMut<NextState<GameMode>>) {
    next_game_mode.set(GameMode::MultiplayerBattleLoading);
}
