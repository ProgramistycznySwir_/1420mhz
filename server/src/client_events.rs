use crate::cannon::{Cannon, Turret, TurretBundle};
use crate::{cannon::cannon_ammo_recharge, cannon::cannon_cooldown};
use crate::{get_player_spawn_location, MapInfo};
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use bevy_replicon::prelude::*;
use bevy_turborand::GlobalRng;
use hydrogen_line_shared::protocol::components::weapons::{NetworkProjectileType, WeaponGroup};
use hydrogen_line_shared::protocol::components::Player;
use hydrogen_line_shared::protocol::events::server::{ChatMessage, ReadyToPlay};
use hydrogen_line_shared::{
    protocol::{
        components::{
            transforms::{InitialNetworkTransform, NetworkRotation},
            weapons::NetworkTurret,
        },
        events::client::{player_movement::PlayerMovement, FireWeaponGroup, SetupPlayer},
    },
    EnginePower, FacingRotation, GyroPower, NetworkProjectileBundle,
};
use hydrogen_line_shared::{PlayerBundle, ShipBundle};
use std::f32::consts::FRAC_PI_2;

pub struct ClientEventsPlugin;

impl Plugin for ClientEventsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            FixedUpdate,
            (
                apply_player_movement,
                apply_fire_weapon
                    .after(cannon_cooldown)
                    .after(cannon_ammo_recharge),
            ),
        );
        app.add_systems(PreUpdate, (apply_setup_player,).after(ServerSet::Receive));
    }
}

fn apply_player_movement(
    mut move_events: EventReader<FromClient<PlayerMovement>>,
    mut players: Query<(
        &Player,
        &mut ExternalImpulse,
        &GlobalTransform,
        &EnginePower,
        &GyroPower,
        &mut FacingRotation,
    )>,
) {
    for FromClient { client_id, event } in move_events.read() {
        for (player, mut ei, transform, engine_power, gyro_power, mut facing_rotation) in
            &mut players
        {
            if *client_id == player.client_id {
                let relative_impulse = Vec2::new(
                    event.throttle_x * engine_power.manouvering_thrusters,
                    event.throttle_y
                        * (if event.throttle_y > 0. {
                            engine_power.main_thruster
                        } else {
                            engine_power.manouvering_thrusters
                        }),
                );

                ei.impulse = Vec2::from_angle(
                    transform
                        .to_scale_rotation_translation()
                        .1
                        .to_euler(EulerRot::XYZ)
                        .2,
                )
                .rotate(relative_impulse);
                ei.torque_impulse = event.rotation * gyro_power.0;

                facing_rotation.0 = event.facing_rotation;
            }
        }
    }
}

fn apply_fire_weapon(
    mut commands: Commands,
    mut events: EventReader<FromClient<FireWeaponGroup>>,
    ships: Query<(&Player, &Transform, &Children, &FacingRotation)>,
    mut turrets: Query<(Entity, &mut Turret, &NetworkTurret, &NetworkRotation)>,
) {
    for FromClient { client_id, event } in events.read() {
        let (player, ship_transform, ship_children, facing_rotation) = ships
            .iter()
            .find(|(player, _, _, _)| &player.client_id == client_id).expect("If there is FireWeaponGroup event, there should be a ship associated with the client");

        for (_, mut turret, network_turret, turret_rotation) in &mut turrets
            .iter_mut()
            .filter(|turret| turret.2.weapon_group == event.0)
            .filter(|turret| ship_children.iter().any(|child| child == &turret.0))
        {
            for _ in 0..turret.cannon.cooldown.times_finished_this_tick() {
                if turret.cannon.ammo <= 0 {
                    continue;
                }

                let global_turret_rotation = turret_rotation.0
                    + ship_transform.rotation.to_euler(EulerRot::XYZ).2
                    + FRAC_PI_2;

                if (global_turret_rotation - facing_rotation.0).abs()
                    < network_turret.aiming_tolerance
                {
                    let velocity = Vec2::from_angle(global_turret_rotation).extend(0.)
                        * turret.cannon.muzzle_velocity;

                    fire_projectile(
                        &mut commands,
                        // &mesh,
                        // &material,
                        ship_transform.translation.xy(),
                        velocity.xy(),
                        turret.cannon.damage,
                        turret.cannon.range,
                        turret.cannon.ignore_rigidbody,
                        network_turret.projectile_type.to_owned(),
                    );

                    turret.cannon.ammo -= 1;
                }
            }
        }
    }
}

/// `position` - Start position of projectile.
/// `ignore_rigidbody` - Entity of rigidbody to ignore (use firing ship most of the time).
fn fire_projectile(
    commands: &mut Commands,
    position: Vec2,
    velocity: Vec2,
    damage: f32,
    range: f32,
    ignore_rigidbody: Option<Entity>,
    projectile_type: NetworkProjectileType,
) {
    commands.spawn(NetworkProjectileBundle::new(
        position,
        velocity,
        damage,
        range,
        ignore_rigidbody,
        projectile_type,
    ));
}

fn apply_setup_player(
    mut commands: Commands,
    mut setup_player_events: EventReader<FromClient<SetupPlayer>>,
    mut chat: EventWriter<ToClients<ChatMessage>>,
    mut ready_to_play_emitter: EventWriter<ToClients<ReadyToPlay>>,
    mut rng: ResMut<GlobalRng>,
    map_info: Res<MapInfo>,
) {
    for FromClient { client_id, event } in setup_player_events.read() {
        let player_name = if let Some(name) = &event.name {
            name.to_owned()
        } else {
            format!("Player {}", client_id.get()).into()
        };

        let pos = get_player_spawn_location(&mut rng, &map_info);

        // Generate pseudo random color from client id.
        let r: f32 = ((client_id.get() % 23) as f32) / 23.0;
        let g = ((client_id.get() % 27) as f32) / 27.0;
        let b = ((client_id.get() % 39) as f32) / 39.0;
        let player_ship = commands
            .spawn(PlayerBundle::new(
                *client_id,
                player_name.to_owned(),
                Vec2::ZERO,
                0.0,
                Color::srgb(r, g, b),
            ))
            .insert(ShipBundle::new(pos))
            .id();

        let main_turret = commands
            .spawn(TurretBundle::new_120mm(WeaponGroup(0), Some(player_ship)))
            .id();

        let pdt = commands
            .spawn(TurretBundle::new_pdt_20mm(
                WeaponGroup(1),
                Some(player_ship),
            ))
            .id();

        commands
            .entity(player_ship)
            .add_child(main_turret)
            .add_child(pdt);

        info!("Spawned default ship for client: {client_id:?}");

        info!("Player [{}] joined the game!", player_name);

        chat.send(ToClients {
            mode: SendMode::Broadcast,
            event: ChatMessage::server(format!("Player [{}] joined the game!", player_name)),
        });

        ready_to_play_emitter.send(ToClients {
            mode: SendMode::Direct(*client_id),
            event: ReadyToPlay,
        });
    }
}
