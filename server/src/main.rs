use std::time::Duration;

use bevy::{
    log::LogPlugin,
    prelude::*,
    winit::{UpdateMode::Continuous, WinitSettings},
};
use bevy_rapier2d::prelude::*;
use bevy_replicon::prelude::*;
use bevy_replicon_renet::RepliconRenetPlugins;
use bevy_turborand::prelude::*;
use client_events::ClientEventsPlugin;
use hydrogen_line_shared::{
    math::lerp,
    protocol::{
        components::{
            transforms::{NetworkPosition, NetworkRotation, NetworkTransform},
            NetworkDurability, NetworkVelocity, Player, Projectile,
        },
        events::server::{ship_destroyed, ShipDestroyed},
        ProtocolPlugin,
    },
    Damage, IgnoreRigidbody, PlayerBundle, RemainingRange, ShipBundle,
};

mod cannon;
mod client_events;
mod networking;
mod setup;

use networking::*;
use setup::SetupPlugin;

#[derive(Resource)]
struct MapInfo {
    asteroid_count: u32,
    radius: f32,
}
impl Default for MapInfo {
    fn default() -> Self {
        Self {
            asteroid_count: 64 * 64,
            radius: 200. * 10.,
        }
    }
}

const FIXED_FRAME_TIME: f32 = 1. / 64.;

fn main() {
    App::new()
        .add_plugins((
            MinimalPlugins,
            LogPlugin {
                ..Default::default()
            },
            RepliconPlugins
                .build()
                .disable::<ClientPlugin>()
                .set(ServerPlugin {
                    tick_policy: TickPolicy::MaxTickRate(60),
                    ..Default::default()
                }),
            RepliconRenetPlugins,
            ProtocolPlugin,
        ))
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0))
        .insert_resource(WinitSettings {
            focused_mode: Continuous,
            unfocused_mode: Continuous,
        })
        .add_plugins(RngPlugin::default())
        .insert_resource(MapInfo::default())
        .add_plugins((ClientEventsPlugin))
        .add_plugins((SetupPlugin, NetworkingPlugin))
        .add_systems(
            FixedUpdate,
            (
                update_network_transforms,
                update_network_rotations,
                projectile_system,
                cannon::cannon_cooldown,
                cannon::cannon_ammo_recharge,
                cannon::aim_turrets_towards_facing_dirr,
                player_death_system,
                hp_regen,
            ),
        )
        .run();
}

fn update_network_transforms(mut players: Query<(&mut NetworkTransform, &Transform)>) {
    for (mut network_transform, transform) in &mut players {
        let new_network_transform = NetworkTransform {
            position: NetworkPosition(transform.translation.xy()),
            rotation: NetworkRotation(transform.rotation.to_euler(EulerRot::XYZ).2),
        };

        network_transform.set_if_neq(new_network_transform);
    }
}

fn update_network_rotations(mut players: Query<(&mut NetworkRotation, &Transform)>) {
    for (mut network_transform, transform) in &mut players {
        let new_network_rotation = NetworkRotation(transform.rotation.to_euler(EulerRot::XYZ).2);

        network_transform.set_if_neq(new_network_rotation);
    }
}

/// System.
pub fn projectile_system(
    mut commands: Commands,
    mut projectiles: Query<
        (
            Entity,
            &mut Transform,
            &Damage,
            &NetworkVelocity,
            &IgnoreRigidbody,
            Option<&mut RemainingRange>,
        ),
        With<Projectile>, // TODO: Probably consolidate Damage, Velocity and such into one component
    >,
    mut entities_with_durability: Query<&mut NetworkDurability>,
    physics: Res<RapierContext>,
) {
    projectiles.iter_mut().for_each(
        |(entity, mut transform, damage, velocity, ignore_rigidbody, remaining_range)| {
            let position_delta = velocity.0 * FIXED_FRAME_TIME;
            let distance = position_delta.length();
            if let Some(mut remaining_range) = remaining_range {
                if remaining_range.dec(distance).is_zero() {
                    commands.entity(entity).despawn_recursive();
                }
            }
            let result = physics.cast_ray(
                transform.translation.xy(),
                velocity.normalize(),
                distance,
                true,
                match ignore_rigidbody.0 {
                    Some(rb) => QueryFilter::new().exclude_rigid_body(rb),
                    None => QueryFilter::default(),
                },
            );
            if let Some((hitted, hit_distance)) = result {
                if let Ok(mut hitted_durability) = entities_with_durability.get_mut(hitted) {
                    hitted_durability.do_damage(&damage);
                };
                commands.entity(entity).despawn_recursive();
            } else {
                transform.translation += position_delta.extend(0.);
            }
        },
    );
}

fn player_death_system(
    mut players: Query<(Entity, &Player, &mut NetworkDurability, &mut Transform)>,
    mut ship_destroyed_events: EventWriter<ToClients<ShipDestroyed>>,
    map_info: Res<MapInfo>,
    mut rng: ResMut<GlobalRng>,
) {
    for (entity, player, mut durability, mut transform) in &mut players {
        if durability.is_min() {
            let pos = get_player_spawn_location(&mut rng, &map_info);

            ship_destroyed_events.send(ToClients {
                mode: SendMode::Broadcast,
                event: ShipDestroyed {
                    client_id: player.client_id,
                    // TODO: Add owner field to projectiles.
                    by: ClientId::SERVER,
                    position: NetworkPosition::from(&*transform),
                },
            });
            transform.translation = pos.extend(0.);
            durability.0.value = durability.0.range.end().to_owned();
        }
    }
}

fn get_player_spawn_location(rng: &mut ResMut<GlobalRng>, map_info: &Res<MapInfo>) -> Vec2 {
    (match rng.u32(0..6) {
        // 0 => Vec2::X,
        // 1 => Vec2::NEG_X,
        // 2 => Vec2::Y,
        // 3 => Vec2::NEG_Y,
        // 4 => Vec2::ONE,
        // 5 => Vec2::NEG_ONE,
        _ => Vec2::ZERO,
    }) * map_info.radius
        * 1.1
}

fn hp_regen(mut query: Query<(&mut NetworkDurability)>) {
    const REGEN_PER_SECOND: f32 = 3.;

    query.iter_mut().for_each(|mut durability| {
        if !durability.0.is_max() {
            let curr_hp = durability.0.value.clone();
            durability
                .0
                .set(curr_hp + REGEN_PER_SECOND * FIXED_FRAME_TIME);
        }
    })
}
