use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use bevy_turborand::prelude::*;
use hydrogen_line_shared::{math::lerp, NetworkAsteroidBundle};

use super::MapInfo;

pub struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_scene);
    }
}

pub fn setup_scene(mut commands: Commands, map_info: Res<MapInfo>, mut rng: ResMut<GlobalRng>) {
    // Spawn asteroids:

    for _ in 0..map_info.asteroid_count {
        let pos_x = lerp(-map_info.radius, map_info.radius, rng.f32());
        let pos_y = lerp(-map_info.radius, map_info.radius, rng.f32());

        // To make asteroids not spawn in proximity of (0, 0).
        if pos_x.abs() < 50.0 && pos_y.abs() < 50.0 {
            continue;
        }

        let position = Vec2::new(pos_x, pos_y);
        let rotation = rng.f32();

        spawn_asteroid(&mut commands, position, rotation);
    }
}

pub(crate) fn spawn_asteroid(commands: &mut Commands, position: Vec2, rotation: f32) {
    // TODO: Make scale a param.
    let scale = Vec3::splat(10.);

    commands
        .spawn(SpatialBundle::from_transform(
            Transform::from_xyz(position.x, position.y, 0.)
                .with_rotation(Quat::from_euler(EulerRot::XYZ, 0., 0., rotation))
                .with_scale(scale),
        ))
        .insert(Collider::cuboid(0.5, 0.5))
        .insert(RigidBody::Dynamic)
        .insert(Dominance { groups: -1 })
        .insert(Damping {
            linear_damping: 0.1,
            angular_damping: 0.1,
        })
        .insert(GravityScale(0.))
        .insert(NetworkAsteroidBundle::new(position, rotation, scale));
}
