use super::FIXED_FRAME_TIME;
use bevy::prelude::*;
use bevy_replicon::{core::Replicated, prelude::ParentSync};
use hydrogen_line_shared::{
    math::move_towards_angle,
    protocol::components::{
        transforms::{InitialNetworkTransform, NetworkRotation},
        weapons::{NetworkProjectileType, NetworkTurret, WeaponGroup},
    },
    FacingRotation,
};

use std::{f32::consts::FRAC_PI_2, time::Duration};

/// System
pub fn cannon_cooldown(mut query: Query<&mut Turret>) {
    query.par_iter_mut().for_each(|mut turret| {
        turret
            .cannon
            .cooldown
            .tick(Duration::from_secs_f32(FIXED_FRAME_TIME));
    });
}

/// System
pub fn cannon_ammo_recharge(mut query: Query<&mut Turret>) {
    query.par_iter_mut().for_each(|mut turret| {
        turret
            .cannon
            .ammo_recharge
            .tick(Duration::from_secs_f32(FIXED_FRAME_TIME));

        turret.cannon.ammo += turret
            .cannon
            .ammo_recharge
            .times_finished_this_tick()
            .min(turret.cannon.ammo_max);
    });
}

/// System
pub fn aim_turrets_towards_facing_dirr(
    ships: Query<(&Transform, &FacingRotation)>,
    mut turrets: Query<
        (
            &Parent,
            &InitialNetworkTransform,
            &mut NetworkRotation,
            &NetworkTurret,
        ),
        Without<FacingRotation>,
    >,
) {
    for (parent, transform, mut network_rotation, turret) in turrets.iter_mut() {
        let (transform, facing_rotation) = ships
            .get(parent.get())
            .expect("Turret needs to be direct child of a ship");

        let relative_facing_dirrection =
            facing_rotation.0 - transform.rotation.to_euler(EulerRot::XYZ).2 - FRAC_PI_2;

        network_rotation.0 = move_towards_angle(
            &network_rotation.0,
            &relative_facing_dirrection,
            &(turret.turret_rotation_speed * FIXED_FRAME_TIME),
        );
    }
}

#[derive(Component)]
pub struct Cannon {
    pub damage: f32,
    pub muzzle_velocity: f32,
    pub cooldown: Timer,
    // TODO LOW: Maybe there are simpler more dynamic ways of solving that.
    pub ignore_rigidbody: Option<Entity>,
    pub range: f32,
    pub ammo: u32,
    pub ammo_max: u32,
    pub ammo_recharge: Timer,
}

#[derive(Component)]
pub struct Turret {
    pub cannon: Cannon,
}

#[derive(Bundle)]
pub struct TurretBundle {
    pub replicated: Replicated,
    // pub spatial_bundle: SpatialBundle,
    pub initial_transform: InitialNetworkTransform,
    pub turret: Turret,
    pub network_turret: NetworkTurret,
    pub network_rotation: NetworkRotation,
    pub parent_sync: ParentSync,
}

impl TurretBundle {
    pub fn new_120mm(weapon_group: WeaponGroup, ignore_rigidbody: Option<Entity>) -> Self {
        let turret_rotation_speed = 45.;
        let aiming_tolerance = 0.5;

        Self {
            replicated: Replicated,
            initial_transform: InitialNetworkTransform::default(),
            turret: Turret {
                cannon: Cannon {
                    damage: 15.,
                    muzzle_velocity: 2000.,
                    cooldown: Timer::from_seconds(1. / 5., TimerMode::Repeating),
                    ignore_rigidbody,
                    range: 1500.,
                    ammo: 5,
                    ammo_max: 5,
                    ammo_recharge: Timer::from_seconds(2., TimerMode::Repeating),
                },
            },
            network_turret: NetworkTurret {
                weapon_group,
                turret_rotation_speed: std::f32::consts::PI * (turret_rotation_speed / 180.),
                aiming_tolerance: std::f32::consts::PI * (aiming_tolerance / 180.),
                projectile_type: NetworkProjectileType::Kinetic120mm,
            },
            network_rotation: NetworkRotation::default(),
            parent_sync: ParentSync::default(),
        }
    }

    pub fn new_pdt_20mm(weapon_group: WeaponGroup, ignore_rigidbody: Option<Entity>) -> Self {
        let turret_rotation_speed = 360.;
        let aiming_tolerance = 5.;

        Self {
            replicated: Replicated,
            initial_transform: InitialNetworkTransform::default(),
            turret: Turret {
                cannon: Cannon {
                    damage: 2.5,
                    muzzle_velocity: 350.,
                    cooldown: Timer::from_seconds(1. / 25., TimerMode::Repeating),
                    ignore_rigidbody,
                    range: 350.,
                    ammo: 20,
                    ammo_max: 20,
                    ammo_recharge: Timer::from_seconds(1. / 5., TimerMode::Repeating),
                },
            },
            network_turret: NetworkTurret {
                weapon_group,
                turret_rotation_speed: std::f32::consts::PI * (turret_rotation_speed / 180.),
                aiming_tolerance: std::f32::consts::PI * (aiming_tolerance / 180.),
                projectile_type: NetworkProjectileType::Kinetic20mm,
            },
            network_rotation: NetworkRotation::default(),
            parent_sync: ParentSync::default(),
        }
    }
}
