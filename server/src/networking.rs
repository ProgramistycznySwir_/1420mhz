use bevy::prelude::*;
use bevy_replicon::prelude::*;
use bevy_replicon_renet::renet::{ConnectionConfig, RenetServer};
use bevy_replicon_renet::{renet::transport::*, *};
use hydrogen_line_shared::protocol::components::Player;
use hydrogen_line_shared::protocol::events::server::ChatMessage;

use std::{
    error::Error,
    net::{Ipv4Addr, SocketAddr, UdpSocket},
    time::SystemTime,
};

pub struct NetworkingPlugin;

impl Plugin for NetworkingPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_networking.map(Result::unwrap))
            .add_systems(FixedUpdate, handle_connections);
    }
}

const PORT: u16 = 1337;
const PROTOCOL_ID: u64 = 0;

fn setup_networking(
    mut commands: Commands,
    channels: Res<RepliconChannels>,
) -> Result<(), Box<dyn Error>> {
    let server_channels_config = channels.get_server_configs();
    let client_channels_config = channels.get_client_configs();

    let server = RenetServer::new(ConnectionConfig {
        server_channels_config,
        client_channels_config,
        ..Default::default()
    });

    let current_time = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?;
    let public_addr = SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), PORT);
    let socket = UdpSocket::bind(public_addr)?;
    let server_config = ServerConfig {
        current_time,
        max_clients: 10,
        protocol_id: PROTOCOL_ID,
        authentication: ServerAuthentication::Unsecure,
        public_addresses: vec![public_addr],
    };
    let transport = NetcodeServerTransport::new(server_config, socket)?;

    commands.insert_resource(server);
    commands.insert_resource(transport);

    info!("Server ready!");

    Ok(())
}

/// Logs server events and spawns a new player whenever a client connects.
fn handle_connections(
    mut server_events: EventReader<ServerEvent>,
    mut chat: EventWriter<ToClients<ChatMessage>>,
    players: Query<&Player>,
) {
    for event in server_events.read() {
        match event {
            ServerEvent::ClientConnected { client_id } => {
                info!("Client [b][{client_id:?}][/b] connected");

                chat.send(ToClients {
                    mode: SendMode::Broadcast,
                    event: ChatMessage::server(format!(
                        "Client [b][{}][/b] connected to server!",
                        client_id.get()
                    )),
                });
            }
            ServerEvent::ClientDisconnected { client_id, reason } => {
                // TODO: Add disconnection logic here
                info!("Client [{client_id:?}] disconnected: {reason}");

                if let Some(player) = players.iter().find(|player| &player.client_id == client_id) {
                    chat.send(ToClients {
                        mode: SendMode::Broadcast,
                        event: ChatMessage::server(format!(
                            "Player [b][{}][/b] left the game!",
                            player.name
                        )),
                    });
                } else {
                    chat.send(ToClients {
                        mode: SendMode::Broadcast,
                        event: ChatMessage::server(format!(
                            "Client [b][{}][/b] disconnected!",
                            client_id.get()
                        )),
                    });
                }
            }
        }
    }
}
