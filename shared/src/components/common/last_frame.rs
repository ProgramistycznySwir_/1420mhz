use bevy::prelude::*;

#[derive(Component)]
pub struct LastFrame<T: Component + Clone>(pub T);

pub fn update_last_frame<T: Component + Clone>(mut query: Query<(&T, &mut LastFrame<T>)>) {
    query
        .par_iter_mut()
        .for_each(|(comp, mut last_frame)| last_frame.0 = comp.clone());
}

impl<T: Component + Clone + PartialEq> LastFrame<T> {
    pub fn has_changed(&self, new: T) -> Option<T> {
        if self.0 != new {
            Some(new)
        } else {
            None
        }
    }
}
