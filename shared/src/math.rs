use std::f32::consts::*;

pub fn lerp<T>(a: T, b: T, t: f32) -> T
where
    T: std::ops::Add<Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Mul<f32, Output = T>
        + std::marker::Copy,
{
    a + (b - a) * t
}

fn f32_mod(t: &f32, length: &f32) -> f32 {
    return (t - (t / length).floor() * length).clamp(0., *length);
}

fn delta_angle(current: &f32, target: &f32) -> f32 {
    let delta = f32_mod(&(target - current), &TAU);

    if delta > PI {
        delta - TAU
    } else {
        delta
    }
}

fn move_towards(current: &f32, target: &f32, max_delta: &f32) -> f32 {
    if &(target - current).abs() <= max_delta {
        *target
    } else {
        current + f32::copysign(1., target - current) * max_delta
    }
}

pub fn move_towards_angle(angle: &f32, target_angle: &f32, max_delta: &f32) -> f32 {
    let delta = delta_angle(angle, target_angle);

    if -max_delta < delta && &delta < max_delta {
        *target_angle
    } else {
        move_towards(angle, &(angle + delta), max_delta)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn move_towards_angle() {
        let angle = 0.0;
        let target_angle = (90. / 360.) * std::f32::consts::PI;
        let speed = 0.1;

        let moved_angle = super::move_towards_angle(&angle, &target_angle, &speed);
        assert!(moved_angle > angle);
        assert_eq!(moved_angle, angle + speed);

        let max_delta = f32::MAX;

        let moved_angle = super::move_towards_angle(&angle, &target_angle, &max_delta);
        assert!(moved_angle > angle);
        assert_eq!(moved_angle, target_angle);

        let target_angle = (-90. / 360.) * std::f32::consts::PI;

        let moved_angle = super::move_towards_angle(&angle, &target_angle, &max_delta);
        assert_eq!(moved_angle, target_angle);
    }

    #[test]
    fn move_towards_angle_propperly_over_0_point() {
        let angle = (270. / 360.) * std::f32::consts::PI;
        let target_angle = (-270. / 360.) * std::f32::consts::PI;
        let max_delta = 0.1;

        let moved_angle = super::move_towards_angle(&angle, &target_angle, &max_delta);
        println!(
            "Moved angle: {}; {} -> {} @ {}",
            moved_angle, angle, target_angle, max_delta
        );
        assert!(moved_angle > angle);
    }
}
