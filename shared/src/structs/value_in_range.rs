use std::ops::{Div, RangeInclusive, Sub};

use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, PartialEq, Debug)]
pub struct ValueInRange<T: Clone + PartialOrd> {
    pub value: T,
    pub range: RangeInclusive<T>,
}

impl<T: Clone + PartialOrd> ValueInRange<T> {
    pub fn from_range(range: RangeInclusive<T>) -> ValueInRange<T> {
        Self {
            value: range.end().clone(),
            range: range,
        }
    }
    pub fn with_value(self: &Self, val: T) -> ValueInRange<T> {
        Self {
            value: val,
            range: self.range.clone(),
        }
    }

    pub fn is_min(self: &Self) -> bool {
        &(self.value) <= self.range.start()
    }
    pub fn is_max(self: &Self) -> bool {
        &(self.value) >= self.range.end()
    }
    /// Ensures that setted value is in range.
    pub fn set(self: &mut Self, val: T) -> &mut Self {
        self.value = if &val > self.range.end() {
            self.range.end().to_owned()
        } else if &val < self.range.start() {
            self.range.start().to_owned()
        } else {
            val
        };
        self
    }
}

impl<T: Clone + PartialOrd + Default> ValueInRange<T> {
    pub fn new(max: T) -> ValueInRange<T> {
        Self::from_range(T::default()..=max)
    }
    pub fn zero(self: &Self) -> ValueInRange<T> {
        Self {
            value: T::default(),
            range: self.range.clone(),
        }
    }
}

impl<T: Clone + PartialOrd + Sub<Output = T> + Div<Output = T>> ValueInRange<T> {
    pub fn percent(&self) -> T {
        (self.value.to_owned() - self.range.start().to_owned()) / self.range.end().to_owned()
    }
}

impl<T: Clone + PartialOrd> From<RangeInclusive<T>> for ValueInRange<T> {
    fn from(value: RangeInclusive<T>) -> Self {
        Self::from_range(value)
    }
}
