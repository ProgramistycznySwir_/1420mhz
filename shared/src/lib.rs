use bevy::prelude::*;
use bevy_rapier2d::{
    dynamics::{Ccd, Damping, ExternalImpulse, GravityScale, RigidBody, Sleeping},
    geometry::{Collider, ColliderMassProperties},
};
use bevy_replicon::prelude::*;
use protocol::components::{
    transforms::{
        InitialNetworkTransform, NetworkPosition, NetworkRotation, NetworkScale, NetworkTransform,
    },
    weapons::NetworkProjectileType,
    Asteroid, NetworkDurability, NetworkVelocity, Player, PlayerColor, Projectile,
};
use structs::value_in_range::ValueInRange;

pub mod components;
pub mod math;
pub mod protocol;
pub mod structs;

use derive_alias::derive_alias;

// pub mod networking;

derive_alias! {
    game_component => #[derive(Component, Reflect, Clone, PartialEq, Debug)],
    network_component => #[derive(Component, Serialize, Deserialize, Reflect, Clone, PartialEq, Debug)],
}

game_component! {
    #[derive(Default)]
    pub struct REEEEEE;
}

#[derive(Bundle)]
pub struct PlayerBundle {
    player: Player,
    network_transform: NetworkTransform,
    color: PlayerColor,
    replicated: Replicated,
}

impl PlayerBundle {
    pub fn new(
        client_id: ClientId,
        name: String,
        position: Vec2,
        rotation: f32,
        color: Color,
    ) -> Self {
        let network_transform = NetworkTransform {
            position: NetworkPosition(position),
            rotation: NetworkRotation(rotation),
        };
        Self {
            player: Player { client_id, name },
            network_transform,
            color: PlayerColor(color),
            replicated: Replicated,
        }
    }
}

#[derive(Component)]
pub struct EnginePower {
    pub main_thruster: f32,
    pub manouvering_thrusters: f32,
}
#[derive(Component, Deref, DerefMut)]
pub struct GyroPower(pub f32);

#[derive(Component, Deref, DerefMut, Default)]
pub struct FacingRotation(pub f32);

#[derive(Bundle)]
pub struct ShipBundle {
    rb: RigidBody,
    damping: Damping,
    tranform: TransformBundle,
    collider: Collider,
    sleeping: Sleeping,
    ccd: Ccd,
    external_impulse: ExternalImpulse,
    mass: ColliderMassProperties,
    gravity: GravityScale,

    engine_power: EnginePower,
    gyro_power: GyroPower,
    facing_rotation: FacingRotation,

    durability: NetworkDurability,
}
impl ShipBundle {
    pub fn new(position: Vec2) -> Self {
        Self {
            rb: RigidBody::Dynamic,
            damping: Damping {
                linear_damping: 0.5,
                angular_damping: 5.0,
            },
            tranform: TransformBundle::from_transform(
                Transform::from_translation(position.extend(0.)).looking_at(Vec3::ZERO, Vec3::Z),
            ),
            collider: Collider::ball(2.),
            sleeping: Sleeping::disabled(),
            ccd: Ccd::enabled(),
            external_impulse: ExternalImpulse::default(),
            mass: ColliderMassProperties::Mass(1.0),
            gravity: GravityScale(0.0),
            engine_power: EnginePower {
                main_thruster: 25. / 15.,
                manouvering_thrusters: 25. / 100.,
            },
            gyro_power: GyroPower(std::f32::consts::TAU * 0.5 / 5.),
            facing_rotation: FacingRotation(0.),
            durability: NetworkDurability(ValueInRange::new(100.)),
        }
    }
}

#[derive(Bundle)]
pub struct NetworkAsteroidBundle {
    asteroid: Asteroid,
    replicated: Replicated,
    network_transform: NetworkTransform,
    network_scale: NetworkScale,
}

impl NetworkAsteroidBundle {
    pub fn new(position: Vec2, rotation: f32, scale: Vec3) -> Self {
        let network_transform = NetworkTransform {
            position: NetworkPosition(position),
            rotation: NetworkRotation(rotation),
        };
        Self {
            asteroid: Asteroid,
            replicated: Replicated,
            network_transform,
            network_scale: NetworkScale(scale),
        }
    }
}

#[derive(Component, Deref, DerefMut, Debug)]
pub struct Damage(pub f32);

#[derive(Component, Deref, DerefMut, Debug)]
pub struct RemainingRange(pub f32);
#[derive(Component, Deref, DerefMut, Debug)]
pub struct IgnoreRigidbody(pub Option<Entity>);

impl RemainingRange {
    pub fn is_zero(self: &Self) -> bool {
        self.0 <= 0.
    }
    pub fn dec(self: &mut Self, distance: f32) -> &mut Self {
        self.0 -= distance;
        self
    }
}

#[derive(Bundle)]
pub struct NetworkProjectileBundle {
    projectile: Projectile,
    replicated: Replicated,
    transform: Transform,
    network_transform: InitialNetworkTransform,
    velocity: NetworkVelocity,
    damage: Damage,
    range: RemainingRange,
    ignore_rigidbody: IgnoreRigidbody,
    projectile_type: NetworkProjectileType,
}

impl NetworkProjectileBundle {
    pub fn new(
        position: Vec2,
        velocity: Vec2,
        damage: f32,
        range: f32,
        ignore_rigidbody: Option<Entity>,
        projectile_type: NetworkProjectileType,
    ) -> Self {
        let rotation = Vec2::Y.angle_between(velocity);
        let network_transform = InitialNetworkTransform(NetworkTransform {
            position: NetworkPosition(position),
            rotation: NetworkRotation(rotation),
        });

        Self {
            projectile: Projectile,
            replicated: Replicated,
            transform: Transform::from_translation(position.extend(0.))
                .with_rotation(Quat::from_euler(EulerRot::XYZ, 0., 0., rotation)),
            network_transform,
            velocity: NetworkVelocity(velocity),
            damage: Damage(damage),
            range: RemainingRange(range),
            ignore_rigidbody: IgnoreRigidbody(ignore_rigidbody),
            projectile_type,
        }
    }
}
