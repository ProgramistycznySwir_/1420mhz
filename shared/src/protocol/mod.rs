use bevy::prelude::*;

use self::{components::ProtocolComponentsPlugin, events::ProtocolEventsPlugin};

pub mod components;
pub mod events;

pub struct ProtocolPlugin;

impl Plugin for ProtocolPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(ProtocolComponentsPlugin);
        app.add_plugins(ProtocolEventsPlugin);
    }
}
