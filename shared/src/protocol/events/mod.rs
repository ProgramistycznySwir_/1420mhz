use bevy::app::Plugin;

use self::{client::ProtocolClientEventsPlugin, server::ProtocolServerEventsPlugin};

pub mod client;
pub mod server;

pub struct ProtocolEventsPlugin;

impl Plugin for ProtocolEventsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins(ProtocolClientEventsPlugin)
            .add_plugins(ProtocolServerEventsPlugin);
    }
}
