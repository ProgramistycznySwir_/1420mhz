use bevy::ecs::event::Event;
use bevy_replicon::core::ClientId;
use serde::{Deserialize, Serialize};

use crate::protocol::components::transforms::NetworkPosition;

/// TODO: Probably change it into normal replication, soo players that just joined in will also see effects
#[derive(Debug, Deserialize, Event, Serialize)]
pub struct ShipDestroyed {
    pub client_id: ClientId,
    pub by: ClientId,
    pub position: NetworkPosition,
}
