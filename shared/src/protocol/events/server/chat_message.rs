use bevy::ecs::event::Event;
use bevy_replicon::core::ClientId;
use serde::{Deserialize, Serialize};

/// TODO: Probably change it into normal replication, soo players that just joined in will also see effects
/// A movement event for the controlled box.
#[derive(Debug, Deserialize, Event, Serialize)]
pub struct ChatMessage {
    pub from: ClientId,
    pub message: String,
}

impl ChatMessage {
    pub fn server(message: String) -> ChatMessage {
        ChatMessage {
            from: ClientId::SERVER,
            message,
        }
    }
}
