pub mod ship_destroyed;
pub use ship_destroyed::*;

pub mod chat_message;
pub use chat_message::*;

pub mod ready_to_play;
pub use ready_to_play::*;

use bevy::app::Plugin;
use bevy_replicon::{core::channels::ChannelKind, prelude::*};

pub struct ProtocolServerEventsPlugin;

impl Plugin for ProtocolServerEventsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app //
            .add_server_event::<ShipDestroyed>(ChannelKind::Unordered)
            .add_server_event::<ChatMessage>(ChannelKind::Ordered)
            .add_server_event::<ReadyToPlay>(ChannelKind::Unordered);
    }
}
