use bevy::prelude::Event;
use serde::{Deserialize, Serialize};

/// ## Event  
/// Sent to player when their ship was instantiated and they are ready to play.
#[derive(Event, Deserialize, Serialize, Debug)]
pub struct ReadyToPlay;
