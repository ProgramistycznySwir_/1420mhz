use bevy::ecs::event::Event;
use serde::{Deserialize, Serialize};

/// A movement event for the controlled box.
#[derive(Debug, Default, Deserialize, Event, Serialize)]
pub struct PlayerMovement {
    pub throttle_x: f32,
    pub throttle_y: f32,
    pub rotation: f32,
    pub facing_rotation: f32,
}
