use bevy::app::Plugin;
use bevy_replicon::{core::channels::ChannelKind, prelude::*};

use self::player_movement::PlayerMovement;

pub mod player_movement;
pub use player_movement::*;
pub mod fire_weapon;
pub use fire_weapon::*;
pub mod setup_player;
pub use setup_player::*;

pub struct ProtocolClientEventsPlugin;

impl Plugin for ProtocolClientEventsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app //
            .add_client_event::<PlayerMovement>(ChannelKind::Unreliable)
            .add_client_event::<FireWeaponGroup>(ChannelKind::Unreliable)
            .add_client_event::<SetupPlayer>(ChannelKind::Unordered);
    }
}
