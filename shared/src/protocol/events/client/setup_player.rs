use bevy::ecs::event::Event;
use serde::{Deserialize, Serialize};

/// A movement event for the controlled box.
#[derive(Debug, Default, Deserialize, Event, Serialize)]
pub struct SetupPlayer {
    pub name: Option<String>,
}
