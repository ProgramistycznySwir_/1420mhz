use bevy::{ecs::event::Event, prelude::*};
use serde::{Deserialize, Serialize};

use crate::protocol::components::weapons::WeaponGroup;

/// In the future weapons groups should be defined by player in the editor, but for now:  
/// `0` - main weapon (high-caliber, powerful, long range and slow)  
/// `1` - secondary weapon (rapid, e.g. PDT)  
/// `2` - special weapon (missiles, grenades, etc.)  
#[derive(Event, Debug, Deref, DerefMut, Deserialize, Serialize)]
pub struct FireWeaponGroup(pub WeaponGroup);

impl FireWeaponGroup {
    pub fn new(weapon_group_id: u8) -> Self {
        Self(WeaponGroup(weapon_group_id))
    }
}
