use bevy::prelude::*;

use bevy_replicon::core::{replication_rules::AppRuleExt, ClientId};
use serde::{Deserialize, Serialize};
use transforms::TransformsPlugin;
use weapons::WeaponsPlugin;

use crate::structs::value_in_range::ValueInRange;

pub mod transforms;
pub mod weapons;

pub struct ProtocolComponentsPlugin;

impl Plugin for ProtocolComponentsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app //
            .add_plugins(TransformsPlugin)
            .add_plugins(WeaponsPlugin)
            .replicate::<Player>()
            .replicate::<PlayerColor>()
            .replicate::<Asteroid>()
            .replicate::<NetworkVelocity>()
            .replicate::<NetworkDurability>()
            .replicate::<Projectile>();
    }
}

/// Contains the client ID of a player.
#[derive(Component, Serialize, Deserialize, Debug)]
pub struct Player {
    pub client_id: ClientId,
    pub name: String,
}

#[derive(Component, Deserialize, Serialize, Deref, DerefMut, Debug)]
pub struct PlayerColor(pub Color);

#[derive(Component, Deserialize, Serialize, Debug)]
pub struct Asteroid;

#[derive(Component, Deserialize, Serialize, Deref, DerefMut, Clone, PartialEq, Debug)]
pub struct NetworkVelocity(pub Vec2);

#[derive(Component, Deserialize, Serialize, Deref, DerefMut, Clone, PartialEq, Debug)]
pub struct NetworkDurability(pub ValueInRange<f32>);

impl NetworkDurability {
    pub fn do_damage(self: &mut Self, dmg: &f32) -> &mut Self {
        self.0.value -= dmg;
        self
    }
}

#[derive(Component, Deserialize, Serialize, Debug)]
pub struct Projectile;
