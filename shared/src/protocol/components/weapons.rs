use bevy::prelude::*;
use bevy_replicon::prelude::*;
use serde::{Deserialize, Serialize};

pub struct WeaponsPlugin;

impl Plugin for WeaponsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.replicate::<NetworkTurret>()
            .replicate::<NetworkProjectileType>();

        #[cfg(debug_assertions)]
        {
            app.register_type::<NetworkTurret>();
        }
    }
}

#[derive(Deserialize, Serialize, Reflect, Debug, Deref, DerefMut, Clone, PartialEq)]
pub struct WeaponGroup(pub u8);

// TODO LOW: You can consider using some kind of `trait` "bundler" and bundle [Component, Deserialize, Serialize] under "NetworkComponent"

#[derive(Component, Deserialize, Serialize, Reflect, Clone, PartialEq, Debug)]
pub struct NetworkTurret {
    pub weapon_group: WeaponGroup,
    /// Radians per second.
    pub turret_rotation_speed: f32,
    /// Arc's angle from targetting direction it is considered good enough for turret to start firing.  
    /// For rapid fire weapons it's recomended to set it to larger values than main weapons.
    pub aiming_tolerance: f32,
    pub projectile_type: NetworkProjectileType,
}

#[derive(Component, Serialize, Deserialize, Reflect, Debug, Clone, PartialEq, Eq)]
pub enum NetworkProjectileType {
    Kinetic20mm,
    Kinetic120mm,
}
