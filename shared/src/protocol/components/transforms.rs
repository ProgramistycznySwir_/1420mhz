use bevy::prelude::*;

use bevy_replicon::core::replication_rules::AppRuleExt;
use serde::{Deserialize, Serialize};

pub struct TransformsPlugin;

// TODO LOW: Check whether this package could help with network synchronization https://github.com/NiseVoid/bevy_bundlication.
impl Plugin for TransformsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app //
            .replicate::<NetworkTransform>()
            .replicate::<NetworkPosition>()
            .replicate::<NetworkRotation>()
            .replicate::<NetworkScale>()
            .replicate::<InitialNetworkTransform>();

        #[cfg(debug_assertions)]
        {
            app //
                .register_type::<NetworkTransform>()
                .register_type::<NetworkPosition>()
                .register_type::<NetworkRotation>()
                .register_type::<NetworkScale>()
                .register_type::<InitialNetworkTransform>();
        }
    }
}

#[derive(Component, Deserialize, Reflect, Serialize, Default, Clone, PartialEq, Debug)]
pub struct NetworkTransform {
    pub position: NetworkPosition,
    pub rotation: NetworkRotation,
}

impl Into<Transform> for &NetworkTransform {
    fn into(self) -> Transform {
        Transform::from_translation(self.position.extend(0.0)).with_rotation(Quat::from_euler(
            EulerRot::XYZ,
            0.0,
            0.0,
            *self.rotation,
        ))
    }
}

#[derive(
    Component, Deserialize, Serialize, Reflect, Default, Deref, DerefMut, Clone, PartialEq, Debug,
)]
pub struct NetworkPosition(pub Vec2);

impl Into<Transform> for &NetworkPosition {
    fn into(self) -> Transform {
        Transform::from_translation(self.0.extend(0.0))
    }
}
impl From<&Transform> for NetworkPosition {
    fn from(value: &Transform) -> Self {
        NetworkPosition(value.translation.xy())
    }
}

#[derive(Component, Deserialize, Serialize, Reflect, Default, Clone, PartialEq, Debug, Deref)]
pub struct NetworkRotation(pub f32);

#[derive(Component, Deserialize, Serialize, Reflect, Deref, DerefMut, Debug)]
pub struct NetworkScale(pub Vec3);

/// Synchronized only once on object creation, ignores further Transform changes.
#[derive(Component, Deserialize, Serialize, Reflect, Default, Deref, DerefMut, Debug)]
pub struct InitialNetworkTransform(pub NetworkTransform);
