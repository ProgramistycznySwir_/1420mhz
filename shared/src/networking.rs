use bevy::{app::Plugin, prelude::App};
use bevy_replicon::core::replication_rules::AppRuleExt;

pub struct SharedNetworkingPlugin;

impl Plugin for SharedNetworkingPlugin {
    fn build(&self, app: &mut App) {
        app.replicate()
    }
}
